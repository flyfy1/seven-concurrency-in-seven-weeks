# What it is About

I first started reading about this book because of a [Weekend
Challenge](http://flyfy1.github.io/activity/2016/03/13/the-weekend-challenge.html).

I wasn't able to complete it at that time.. However, "reading a book" has became
a habit of mine, since then.

And this book is really nice to read about.

So let me just continue building this repo.
