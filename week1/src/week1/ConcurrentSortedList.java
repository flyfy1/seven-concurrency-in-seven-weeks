package week1;

import java.util.concurrent.locks.ReentrantLock;

/**
 * TODO: I haven't fully understand this model yet.
 */
public class ConcurrentSortedList {
    private class Node {
        int value;
        Node prev;
        Node next;

        ReentrantLock lock = new ReentrantLock();

        Node() {
        }

        Node(int v, Node p, Node n) {
            this.value = v;
            this.prev = p;
            this.next = n;
        }

        @Override
        public String toString() {
            return "Node Value: '" + value + "'.";
        }
    }

    private final Node head;
    private final Node tail;

    public ConcurrentSortedList() {
        this.head = new Node();
        this.tail = new Node();
        head.next = tail;
        tail.prev = head;
    }

    public void insert(int v) {
        System.out.println("Inserting: " + v);
        Node current = this.head;

        current.lock.lock();
        System.out.println("L1, Locked Node: " + current);

        Node next = current.next;

        try {
            while (true) {
                next.lock.lock();
                System.out.println("L2, Locked Node: " + next);
                try {
                    if (next == tail || next.value < v) {
                        Node node = new Node(v, current, next);
                        next.prev = node;
                        current.next = node;
                        return;
                    }
                } finally {
                    System.out.println("L2, Unlock Node: " + current);
                    current.lock.unlock();
                }

                current = next;
                next = current.next;
            }
        } finally {
            System.out.println("L1, Unlock Node: " + next);
            next.lock.unlock();
        }
    }

    public int size() {
        Node current = tail;
        int count = 0;

        while (current.prev != head) {
            ReentrantLock lock = current.lock;
            lock.lock();

            try {
                ++count;
                current = current.prev;
            } finally {
                lock.unlock();
            }
        }

        return count;
    }

    public static void main(String[] args) throws InterruptedException {
        final ConcurrentSortedList list = new ConcurrentSortedList();

        Thread t = new Thread() {
            public void run() {
                list.insert(10);
                list.insert(20);
                list.insert(30);
            }
        };
        t.start();
        list.insert(25);
        list.insert(15);
        list.insert(15);
        t.join();
        System.out.println("Current size of list: " + list.size());
    }
}
