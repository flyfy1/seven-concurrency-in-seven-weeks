package week1;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by songyy on 12/3/16.
 */
public class Philosopher extends Thread{
    private ReentrantLock leftChopstick, rightChopstick;
    private Random random;

    public Philosopher(ReentrantLock leftChopstick, ReentrantLock rightChopstick){
        this.leftChopstick = leftChopstick; this.rightChopstick = rightChopstick;
        random = new Random();
    }

    public void run(){
        try{
            while(true){
                Thread.sleep(random.nextInt(1000));
                leftChopstick.lock();

                try{
                    if(rightChopstick.tryLock(1000, TimeUnit.MILLISECONDS)){
                        try{
                            Thread.sleep(random.nextInt(1000));
                        } finally {
                            rightChopstick.unlock();
                        }
                    } else{
                        // didn't get the right chopstick, so giveup
                    }
                } finally {
                    leftChopstick.unlock();
                }
            }
        } catch (InterruptedException e){}
    }
}
