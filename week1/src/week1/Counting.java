package week1;

/**
 * Created by songyy on 22/2/16.
 */
public class Counting {
    public static void main(String[] args) throws InterruptedException {
        class Counter{
            private int count = 0;
            public synchronized void inc(){++count; }
            public int getCount(){return count;}
        }

        final Counter counter = new Counter();

        class CountingThread extends Thread{
            public void run(){
                for(int x = 0 ; x < 10000; ++x) counter.inc();
            }
        }

        CountingThread t1 = new CountingThread();
        CountingThread t2 = new CountingThread();

        t1.start(); t2.start();
        t1.join(); t2.join();

        System.out.printf(String.valueOf(counter.getCount()));
    }
}
