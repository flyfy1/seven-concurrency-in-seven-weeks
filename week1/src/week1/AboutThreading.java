package week1;

/**
 * Created by songyy on 22/2/16.
 */
public class AboutThreading {
    public static void main(String[] args) throws InterruptedException {
        Thread myThread = new Thread(){
            public void run(){
                System.out.println("Hello from new thread!");
            }
        };

        myThread.start();
        Thread.yield();
        System.out.println("Hello from main thread.");
        myThread.join();
    }
}
