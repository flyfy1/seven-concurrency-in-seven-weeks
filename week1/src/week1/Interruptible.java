package week1;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by songyy on 11/3/16.
 */
public class Interruptible {
    public static void output_lock(String msg, ReentrantLock l1, ReentrantLock l2) {
        System.out.println(msg + "l1 is locked: " + l1.isLocked() + ", l2 is locked: " + l2.isLocked());
    }

    public static void main(String[] args) throws InterruptedException{
        final ReentrantLock l1 = new ReentrantLock();
        final ReentrantLock l2 = new ReentrantLock();

        Thread t1 = new Thread(){
            public void run(){
                try{
                    l1.lockInterruptibly();
                    output_lock("t1, l1.lockInterruptibly() - " ,l1, l2);

                    Thread.sleep(1000);
                    l2.lockInterruptibly();
                    output_lock("t1, l2.lockInterruptibly() - " ,l1, l2);
                }catch (InterruptedException e){
                    System.out.println("t1 interrupted.");
                }
            }
        };

        Thread t2 = new Thread(){
            public void run(){
                try{
                    l2.lockInterruptibly();
                    output_lock("t2, l1.lockInterruptibly() - " ,l1, l2);

                    Thread.sleep(1000);
                    l1.lockInterruptibly();

                    output_lock("t2, l2.lockInterruptibly() - " ,l1, l2);
                } catch (InterruptedException e){
                    System.out.println("t2 interrupted");
                }
            }
        };

        t1.start(); t2.start();
        Thread.sleep(2000);
        t1.interrupt(); t2.interrupt();
        t1.join(); t2.join();
    }
}
