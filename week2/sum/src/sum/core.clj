(ns sum.core
  (:gen-class))

(defn reduce-sum [numbers]
  (reduce (fn [acc x] (+ acc x)) 0 numbers)
)

(defn sum [numbers]
  (reduce + numbers))

(defn apply-sum [numbers]
  (apply + numbers))

(ns sum.core
  (:require [clojure.core.reducers :as r]))

(defn parallel-sum [numbers]
  (r/fold + numbers))

(defn -main
  "Just a simple main func"
  [& args]
  (println "Hello, World!")
  (println "Hello, World 2!")
)
