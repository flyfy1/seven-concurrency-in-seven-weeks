(ns word-count.core
  (:gen-class))

(defn word-frequencies [words]
  (reduce
    (fn [counts word] (assoc counts word (inc (get counts word 0))))
  {} words))

(defn get-words [text] (re-seq #"\w+" text))

(defn count-words-sequential [pages]
  (frequencies (mapcat get-words pages)))

(defn count-words-parallel [pages]
  (reduce 
    (partial merge-with +)  ; the merge-counts
    (pmap #(frequencies (get-words %)) pages)))


; It's said to be 3.2x speedup via this method 
(defn count-words-batch [pages]
  (reduce 
    (partial merge-with +)
    (pmap count-words-sequential ; count words per 100 page batch
          (partition-all 100 pages) 
          )))

(defprotocol CollReduce (coll-reduce [coll f] [coll f init]))

(defn my-reduce
  ([f coll] (coll-reduce coll f))
  ([f init coll] (coll-reduce coll f init)))

(defn make-reducer [reducible transformf]
  (reify
    CollReduce
    (coll-reduce [_ f1]
      (coll-reduce reducible (transformf f1) (f1)))
    (coll-reduce [_ f1 init]
      (coll-reduce reducible (transformf f1) (init)))
  ))

(defn my-map [mapf reducible]
  (make-reducer reducible
                (fn [reducef]
                  (fn [acc v]
                    (reducef acc (mapf v))))
  ))


(defn -main
  "main func to test out the word-frequencies func"
  [& args]
  (print (word-frequencies ["one" "potato" "two" "potato" "three" "potato"]))
  )
