(ns clojure-learn.core
  (:gen-class))

(defn favorite-things [name & things]
  (str "Hi, " name ", here are my favorite things: " (clojure.string/join ", " things))
)

(defn my-first [[first-thing second-thing]] second-thing)

; destructure arrays
(defn chooser [[first-choice second-choice & unimportant-choices]]
    (println (str "Your first choice is: " first-choice))
    (println (str "Your second choice is: " second-choice))
    (println (str "We're ignoring the rest of your choices. "
                                  "Here they are in case you need to cry over them: "
                                  (clojure.string/join ", " unimportant-choices))))

; destructure maps
(defn announce-treasure-location
       [{lat :lat lng :lng}]
       (println (str "Treasure lat: " lat))
       (println (str "Treasure lng: " lng)))

; retain original
(defn receive-treasure-location
    [{:keys [lat lng] :as treasure-location}]

    (println (str "Treasure lat: " lat))
    (println (str "Treasure lng: " lng))

    ;; One would assume that this would put in new coordinates for your ship
    (println "original is: " treasure-location))


(defn -main
  "I don't do a whole lot ... yet."
  [& args]

  ; annoymous function
  (clojure.string/join "; "
             (map (fn [name] (str "Hi, " name)) ["Darth Vader" "Mr.Magoo"]))

  ; # annoymous function
  (println (#(+ (* %1 8) %2) 3 4))
)
