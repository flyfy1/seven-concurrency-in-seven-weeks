defmodule Counter do
  def loop(count) do
    receive do
      {:next, sender, ref} -> 
        send(sender, {:ok, ref, count})
        loop(count + 1)
    end
  end

  def start(count) do
    spawn(__MODULE__, :loop, [count])
  end

  def next(counter) do
    ref = make_ref()
    send(counter, {:next, self(), ref})

    receive do
      {:ok, ^ref, count} -> count
    end
  end

end

# counter = spawn(Counter, :loop, [1])
# send(counter, {:next})
# send(counter, {:next})
# send(counter, {:next})

counter = Counter.start(42)
IO.puts Counter.next(counter)
IO.puts Counter.next(counter)
