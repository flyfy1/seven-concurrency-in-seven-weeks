defmodule Counter do
  def loop(count) do
    receive do
      {:next} -> 
        IO.puts "Current count: #{count}"
        loop(count + 1)
    end
  end

  def start(count) do
    spawn(__MODULE__, :loop, [count])
  end

  def next(counter) do
    send(counter, {:next})
  end

end

# counter = spawn(Counter, :loop, [1])
# send(counter, {:next})
# send(counter, {:next})
# send(counter, {:next})

counter = Counter.start(42)
Counter.next(counter)
Counter.next(counter)
